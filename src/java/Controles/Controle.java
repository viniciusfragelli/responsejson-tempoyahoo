/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.json.JSONObject;

/**
 *
 * @author Vinicius
 */
public class Controle {
    
    private JSONObject object;
    
    public String getImagem(){
        //return object.optJSONObject("query").optJSONObject("results").optJSONObject("channel").optJSONObject("image").getString("link");
        int k = object.toString().indexOf("CDATA[<img src=\\\"");
        String aux = object.toString().substring(k).replace("CDATA[<img src=\\\"", "");
        aux = aux.substring(0,aux.indexOf(".gif")+4);
        System.out.println(aux);
        return aux;
    }
    
    public void carregaInfo(String cidade, String uf){
        cidade = cidade.replace(" ", "%20");
        uf = uf.replace(" ", "%20");
        object = getObject("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+cidade+"%2C%20"+uf+"%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
    }
    
    public String getFahrenheit(){
        int f = Integer.parseInt(object.optJSONObject("query").optJSONObject("results").optJSONObject("channel").optJSONObject("item").optJSONObject("condition").getString("temp"));
        if(f >= 0){
            return "<font color=\"green\">"+f+"</font>";
        }else{
            return "<font color=\"red\">"+f+"</font>";
        }
    }
    
    public String getCelsius(){
        int f = Integer.parseInt(object.optJSONObject("query").optJSONObject("results").optJSONObject("channel").optJSONObject("item").optJSONObject("condition").getString("temp"));
        int c = (int) ((f - 32)/1.8);
        if(c >= 0){
            return "<font color=\"green\">"+c+"</font>";
        }else{
            return "<font color=\"red\">"+c+"</font>";
        }
    }
    
    public JSONObject getObject(String url){
        int timeout = 5000;
        HttpURLConnection c = null;
        JSONObject ob = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();
            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    String sb = "";
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb += line;
                    }
                    br.close();
                    System.out.println(sb);
                    ob = new JSONObject(sb);
            }

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (ProtocolException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return ob;
    }
    
}
