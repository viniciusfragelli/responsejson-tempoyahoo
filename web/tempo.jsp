<%-- 
    Document   : tempo.jsp
    Created on : 09/11/2016, 08:25:29
    Author     : Vinicius
--%>

<%@page import="Controles.Controle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tempo Yahoo</title>
    </head>
    <body>
        <%
            Controles.Controle controle = new Controle();
            String cidade = request.getParameter("cidade");
            String estado = request.getParameter("estado");
            controle.carregaInfo(cidade, estado);
        %>
        <h1>
            <form action="tempo.jsp">
            Previsão do tempo:</br>
            Cidade: <input type="text" name="cidade" value="<%out.println(cidade);%>" /><br>
            Estado: <input type="text" name="estado" value="<%out.println(estado);%>" /><br>
            <img src="<%out.print(controle.getImagem());%>"/><br><br>
            Temperatura:<br>
            <%out.print(controle.getFahrenheit());%> ºF<br>
            <%out.print(controle.getCelsius());%> ºC<br><br>
            <input type="submit" value="Ver tempo" />
            </form>
        </h1>
    </body>
</html>
